/****************************************************************************************************************************
@Author Cognizant
@name CaseTrigger 
@Description Case Trigger to create task record on after insert of case record
@Version 40.0 
*/
trigger CaseTrigger on Case (after insert) {
    List<Case> caseList = new List<Case>();
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            for (Case objcase: Trigger.New) {
                caseList.add(objcase);    
            }    
        }    
    }
    if(!caseList.isEmpty()){
        CaseHelper.Createtasks(caseList);    
    }

}