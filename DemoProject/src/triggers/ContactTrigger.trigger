/****************************************************************************************************************************
@Author Cognizant
@name ContactTrigger 
@Description Contact Trigger to create case record on after insert of contact record
@Version 40.0 
*/
trigger ContactTrigger on Contact (after insert) {
    List<Contact> contactsList = new List<Contact>();
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            for (Contact con: Trigger.New) {
                contactsList.add(con);    
            }    
        }    
    }
    if(!contactsList.isEmpty()){
        ContactHelper.Createcases(contactsList);    
    }

}