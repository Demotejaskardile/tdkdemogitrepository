/****************************************************************************************************************************
@Author Cognizant
@name ContactTestClass 
@Description ContactTestClass class which will used in covering contact trigger and its handler 
@Version 40.0 
*/
@isTest
public class ContactTestClass {
  
    /************************************************************************************************************************
    * Methodname: test1
    * @Description: This static method will be used for covering test coverage for contact trigger and its handler
    * Retuntype: Void
    */   
    static testmethod void test1() {
        TestDataFactory.createContactTestRecords(1,2000);
        // Run some tests
    }
}