public class MultiSelectPicklistCls {
    @AuraEnabled
    public static List<String> getPicklistvalues(sObject objName, String field_apiname){
        List<String> optionlist = new List<String>();
        
        Schema.sObjectType sobject_type = objName.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_apiname).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : pick_list_values) {
            optionlist.add(a.getValue());
        }
        return optionlist;
    }
}