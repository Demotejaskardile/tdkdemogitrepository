public class Send_SMS_Controller {
    
    @AuraEnabled
    public static string SendMessage(String Phonenumber, String PhoneMessage) {
        
        String successMsg = send(Phonenumber, PhoneMessage);
        return successMsg;
    }
    
    private static final String ENDPOINT = 'https://api.twilio.com';
   // private static final String VERSION = '2010-04-01';
    
    public Static string send(String toNumber1, String message1) {
        String accountsid = 'ACa9d431b231666c6c94ee7aabdc2ba0e2';
        String authToken = 'dfb3199df2a3f87cbe59a92782510bf1';
        String fromPhNumber1 = '+1 (775) 553-5569';
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.twilio.com/2010-04-01/Accounts/'+accountSid+'/SMS/Messages.json');
        req.setMethod('POST');
        String VERSION  = '3.2.0';
        req.setHeader('X-Twilio-Client', 'salesforce-' + VERSION);
        req.setHeader('User-Agent', 'twilio-salesforce/' + VERSION);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Accept-Charset', 'utf-8');
        req.setHeader('Authorization','Basic '+EncodingUtil.base64Encode(Blob.valueOf(accountsid+':' +authToken)));
        req.setBody('To='+EncodingUtil.urlEncode(toNumber1,'UTF-8')+'&From='+EncodingUtil.urlEncode(fromPhNumber1,'UTF-8')+'&Body='+message1);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug(res.getBody());
        string result;
        if (res.getStatusCode() >= 200 && res.getStatusCode() < 300) {
            result = 'SMS Send Sucessfully';
        } else {
            result = 'SMS Sending failed, Contact to Administrator';
        }
        return result;
    }
}