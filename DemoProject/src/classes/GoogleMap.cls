public with sharing class GoogleMap {
	@AuraEnabled
    public static List<Account> getAccounts(){
        //https://releasenotes.docs.salesforce.com/en-us/summer16/release-notes/rn_general_geocodes_aloha.htm
        //https://releasenotes.docs.salesforce.com/en-us/summer16/release-notes/data_dot_com_clean_add_geocode_information_to_all_records.htm
        //https://help.salesforce.com/articleView?id=data_dot_com_clean_admin_clean_rules.htm&type=0&language=en_US
        return [select id, name, BillingLatitude, BillingLongitude from Account where BillingLatitude != null and BillingLongitude != null];
    }
    public static string concat(string address)
    {
        string objaddress='';
        list<string> lststr = address.split(' ');
        
        return objaddress;
    }
    @AuraEnabled
    public static List<MapWrapper> getContacts(Id AccountID){
        List<MapWrapper> objlstMap =  new List<MapWrapper>();
        System.debug('=========' +AccountID);
        //https://releasenotes.docs.salesforce.com/en-us/summer16/release-notes/rn_general_geocodes_aloha.htm
        //https://releasenotes.docs.salesforce.com/en-us/summer16/release-notes/data_dot_com_clean_add_geocode_information_to_all_records.htm
        //https://help.salesforce.com/articleView?id=data_dot_com_clean_admin_clean_rules.htm&type=0&language=en_US
        List<Contact> lstcon = [select id, name, 
                                MailingLatitude,MailingLongitude,
                                MailingStreet,MailingCity,
                                MailingState, MailingCountry,
                                MailingPostalCode
                                from Contact where AccountId=:AccountID];
        
        for(Contact con: lstcon)
        {
            string address='';
            string strURL = '';
            if(con.MailingStreet!=null){
                address += con.MailingStreet+',';
                strURL += concat(con.MailingStreet) + ',';
            }
            if(con.MailingCity !=null){
                address += con.MailingCity+',';
            }
            if(con.MailingState !=null){
                address +=con.MailingState+',';
            }
            if(con.MailingCountry!=null){
                address += con.MailingCountry+',';
            }
            if(con.MailingPostalCode!=null){
                address += con.MailingPostalCode+',';
            }
            
            /* if address is not blank then get the latitude and longitude through google api */
            if(address !=''){
                HttpRequest req = new HttpRequest();
                /* Encode address */
                address = EncodingUtil.urlEncode(address,'UTF-8');
                req.setEndPoint('https://maps.googleapis.com/maps/api/geocode/xml?address='+address+'&sensor=true');
                req.setMethod('GET');
                System.debug('=====req'+ req);
                Http http = new Http();
                HttpResponse res;
                if(!Test.isRunningTest()){
                    res = http.send(req);
                }else{
                    /* create sample data for test method */
                    String resString = '<GeocodeResponse><status>OK</status><result><geometry><location><lat>37.4217550</lat> <lng>-122.0846330</lng></location>';
                    resString +='</geometry> </result> </GeocodeResponse>';
                    res = new HttpResponse();
                    res.setBody(resString);
                    res.setStatusCode(200);
                    
                }
                
                Dom.Document doc = res.getBodyDocument();   
                System.debug('======doc===' +doc);
                /* Get the root of xml response */
                Dom.XMLNode geocodeResponse = doc.getRootElement();
                if(geocodeResponse!=null){
                    /* Get the result tag of xml response */
                    Dom.XMLNode result = geocodeResponse.getChildElement('result',null);
                    if(result!=null){
                        /* Get the geometry tag  of xml response */
                        Dom.XMLNode geometry = result.getChildElement('geometry',null);
                        if(geometry!=null){
                            /* Get the location tag  of xml response */
                            Dom.XMLNode location = geometry.getChildElement('location',null);
                            if(location!=null){
                                /* Get the lat and lng tag  of xml response */
                                String lat = location.getChildElement('lat', null).getText();
                                String lng = location.getChildElement('lng', null).getText();
                                try{
                                    //acc.BillingLatitude =Decimal.valueof(lat);
                                    //acc.BillingLongitude =Decimal.valueof(lng);
                                    if(lat != '' && lng != '')
                                    {
                                         MapWrapper objMap =  new MapWrapper();
                                        objMap.dLatitude = Decimal.valueof(lat);
                                        objMap.dLongitude = Decimal.valueof(lng);
                                        objMap.objcon = con;
                                        objlstMap.add(objMap);
                                        System.debug('======>'+objlstMap);    
                                    }
                                    
                                    
                                }catch(Exception ex){
                                    system.debug('Exception '+ex.getMessage());
                                }
                            }
                        }
                    }
                }
            }
            
            
            
        }
        //return [select id, name, MailingLatitude, MailingLongitude from Contact where AccountId=:AccountID];
          // create a instance of wrapper class.
        
        return objlstMap;
        
    }
    
    
    // create a wrapper class with @AuraEnabled Properties    
    public class MapWrapper {
        @AuraEnabled public Decimal  dLatitude {get;set;}
        @AuraEnabled public Decimal dLongitude {get;set;}
        @AuraEnabled public Contact objcon {get;set;}
        @AuraEnabled public URL strURL {get;set;}
       /* Public MapWrapper (Contact objcon, Decimal dLongi, Decimal dLati)
        {
            dLongitude = dLongi;
            dLatitude = dLati;
            objcon = objcon;
        }*/
    }
    
}