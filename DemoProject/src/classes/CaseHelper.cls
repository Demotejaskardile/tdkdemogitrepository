/****************************************************************************************************************************
@Author Cognizant
@name CaseHelper 
@Description Case trigger Handler 
@Version 40.0 
*/
public class CaseHelper {

    /************************************************************************************************************************
    * Methodname: Createtasks
    * @Description: This static method will be used for creating tasks record  
    * Retuntype: Void
    */ 
    public static void Createtasks(List<Case> casesList) {
        // get all the level data from the custom setting
        Map<String, Static_Data__c> level = Static_Data__c.getAll();
		Map<ID,Contact> ownermapbycontactID = new Map<ID,Contact>([Select id, OwnerID from Contact Limit 50000]);
        List<Task> tasksToInsert = new List<Task>();
        for(Case objcase : casesList){
            //create Task
            Task t = new Task();
            //enter details
            if(ownermapbycontactID.containsKey(objcase.ContactId))
            t.OwnerId = ownermapbycontactID.get(objcase.ContactId).OwnerID;
            t.WhatID =   objcase.ID;
            if(level.containsKey('TaskPriority'))
            t.Priority = level.get('TaskPriority').Value__c;
            if(level.containsKey('TaskStatus'))
            t.Status = level.get('TaskStatus').Value__c;
            if(level.containsKey('TaskSubject'))
            t.Subject = level.get('TaskSubject').Value__c+' ' + objcase.CaseNumber;
            t.ActivityDate = Date.Today() + Integer.valueOf(level.get(objcase.Priority).Value__c);
            tasksToInsert.add(t);   
        }
        if(!tasksToInsert.isEmpty()){
            insert tasksToInsert;    
        }
    }
}