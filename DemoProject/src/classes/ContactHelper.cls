/****************************************************************************************************************************
@Author Cognizant
@name ContactHelper 
@Description Contact trigger Handler 
@Version 40.0 
*/
public class ContactHelper {
    
    /************************************************************************************************************************
    * Methodname: Createcases
    * @Description: This static method will be used for creating case record  
    * Retuntype: Void
    */ 
    public static void Createcases(List<Contact> contactsList) {
        // get all the level data from the custom setting
        Map<String, Static_Data__c> level = Static_Data__c.getAll();
        
        List<Case> casesToInsert = new List<Case>();
        for(Contact con : contactsList){
            //create case
            Case c = new Case();
            //enter details
            c.OwnerId = con.OwnerId;
            if(con.AccountId != null) 
                c.AccountId = con.AccountId;
            if(level.containsKey(con.Level__c))
                c.Priority = level.get(con.Level__c).Value__c;
            c.ContactId = con.Id;
            if(level.containsKey('CaseOrigin'))
                c.Origin = level.get('CaseOrigin').Value__c;
            if(level.containsKey('CaseStatus'))
                c.Status = level.get('CaseStatus').Value__c;
            casesToInsert.add(c);   
        }
        if(!casesToInsert.isEmpty()){
            insert casesToInsert;    
        }
    }
    
}