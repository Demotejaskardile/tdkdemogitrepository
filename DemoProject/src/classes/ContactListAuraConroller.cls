public with Sharing class ContactListAuraConroller {

    @AuraEnabled
    public static List<Contact> getContactInfo(String accountId)
    {
        system.debug('========'+accountId);
        return [select Id, Name from Contact where AccountId =: accountId];
    }
}