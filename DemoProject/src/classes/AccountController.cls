public class AccountController {
    private static final string  SUCCESS_MESSAGE = 'Record created successfully';
     @AuraEnabled public String successMsg; //success msg
    @AuraEnabled
    public static Account saveAccount(Account acc) {
        
        // Perform isUpdateable() check here
        upsert acc;
        return acc;
    }
    
    @AuraEnabled
    public static Account getAccount(ID accID) {
        return [SELECT Id, Name, AccountNumber, Phone, Parent.ID,Parent.Name, SLAExpirationDate__c FROM Account
                where ID =: accID];
    }
    
    
    @AuraEnabled
    public static AccountPagerWrapper fetchAccount(Decimal pageNumber ,Integer recordToDisply) {
        Integer pageSize = recordToDisply;
        Integer offset = ((Integer)pageNumber - 1) * pageSize;
        // create a instance of wrapper class.
        AccountPagerWrapper obj =  new AccountPagerWrapper();
        // set the pageSize,Page(Number), total records and accounts List(using OFFSET)   
        obj.pageSize = pageSize;
        obj.page = (Integer) pageNumber;
        obj.total = [SELECT count() FROM account];
        obj.accounts = [SELECT Id, Name,Phone,AccountNumber,SLAExpirationDate__c	
                        FROM Account ORDER BY CreatedDate desc LIMIT :recordToDisply  OFFSET :offset ];
        // return the wrapper class instance .
        return obj;
    }
    
    // create a wrapper class with @AuraEnabled Properties    
    public class AccountPagerWrapper {
        @AuraEnabled public Integer pageSize {get;set;}
        @AuraEnabled public Integer page {get;set;}
        @AuraEnabled public Integer total {get;set;}
        @AuraEnabled public List<Account> accounts {get;set;}
    }
}