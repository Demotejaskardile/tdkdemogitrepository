public class ActivityController {
	public String recordId { get; set; }
    Public List<Activitywrapper> lstActwrp { get; set; }
    public ActivityController()
    {
        recordId = ApexPages.currentPage().getParameters().get('id');
        lstActwrp = getdata();
        //Select (Select o.What.Name , o.WhatId, o.IsTask, o.IsEvent__c, o.ActivityDate, o.Account.Name From OpenActivities o) from account  where ID=:account.id];
       
        
    }
    
    Public List<Activitywrapper> getdata()
    {
        list<Activitywrapper> lstobjacwrp = new list<Activitywrapper>();
        for (Task task: [Select ID, Subject, Status, Account.Name From Task where AccountID =: recordId])
        {
            Activitywrapper objacwrp = new Activitywrapper();
            objacwrp.Subject = task.Subject;
            objacwrp.Status = task.Status;
            objacwrp.WhatName = task.Account.Name;
            objacwrp.id = task.Id;
            
            lstobjacwrp.add(objacwrp);
        }
        return lstobjacwrp;
    }
    
    public class  Activitywrapper{
        
        public String Subject { get; set; }
        public String Status { get; set; }
        public String WhatName { get; set; }
        public String id { get; set; }
        
    }
}