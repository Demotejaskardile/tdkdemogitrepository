public class SampleGoogleMapController {
    @AuraEnabled
    public static List<Account> getAccounts(){
        return [select id, name,  ShippingLatitude, ShippingLongitude from Account 
                where ShippingLatitude != null and ShippingLongitude != null];
    }
}