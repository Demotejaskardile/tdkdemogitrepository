/****************************************************************************************************************************
@Author Cognizant
@name TestDataFactory 
@Description TestDataFactory class which will used in test class and will create account and contact records 
@Version 40.0 
*/
@isTest
public class TestDataFactory {
    
    /************************************************************************************************************************
    * Methodname: createTestRecords
    * @Description: This static method will be used for creating test data
    * Retuntype: Void
    */ 
    public static void createContactTestRecords(Integer numAccts, Integer numContactsPerAcct) {
		/*Insert data into Custom setting*/
        list<Static_Data__c> lstCPM =new list<Static_Data__c>();//bulk List of custom setting object for bulk insert
        
        Static_Data__c cpm=new Static_Data__c(); //Custom Setting for case object
        cpm.Name='Primary';//Static record 1 of custom setting
        cpm.Value__c = 'High';
        lstCPM.add(cpm);
        
        Static_Data__c cpm1=new Static_Data__c(); //Custom Setting for case object
        cpm1.Name='CaseOrigin';//Static record 2 of custom setting
        cpm1.Value__c = 'New Contact';
        lstCPM.add(cpm1);
        
        Static_Data__c cpm2=new Static_Data__c(); //Custom Setting for case object
        cpm2.Name='CaseStatus';//Static record 3 of custom setting
        cpm2.Value__c = 'Working';
        lstCPM.add(cpm2);
        
        Static_Data__c cpm3=new Static_Data__c(); //Custom Setting for case object
        cpm3.Name='TaskPriority';//Static record 4 of custom setting
        cpm3.Value__c = 'Normal';
        lstCPM.add(cpm3);
        
        Static_Data__c cpm4=new Static_Data__c(); //Custom Setting for case object
        cpm4.Name='TaskStatus';//Static record 5 of custom setting
        cpm4.Value__c = 'Not Started';
        lstCPM.add(cpm4);
        
        Static_Data__c cpm5=new Static_Data__c(); //Custom Setting for case object
        cpm5.Name='TaskSubject';//Static record 6 of custom setting
        cpm5.Value__c = 'Welcome call for';
        lstCPM.add(cpm5);
        
        Static_Data__c cpm6=new Static_Data__c(); //Custom Setting for case object
        cpm6.Name='High';//Static record 7 of custom setting
        cpm6.Value__c = '7';
        lstCPM.add(cpm6);
        
        insert lstCPM;
        
        /*Account Insert Code*/
        List<Account> accts = new List<Account>();
        
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestAccount' + i);
            accts.add(a);
        }
        
        try{
            System.assert(true, accts!=null); 
            insert accts;    
        }
        catch(System.DMLException e){
            
        }

        
        
        
        
        List<Contact> cons = new List<Contact>();
        for (Integer j=0;j<numAccts;j++) {
            Account acct = accts[j];            
            // For each account just inserted, add contacts
            for (Integer k=numContactsPerAcct*j;k<numContactsPerAcct*(j+1);k++) {
                cons.add(new Contact(firstname='Test'+k,
                                     lastname='Test'+k,
                                     Level__c='Primary',
                                     AccountId=acct.Id));
            }
        }
        // Insert all contacts for all accounts
        try{
            System.assert(true, cons!=null); 
            insert cons;
        }
        catch(System.DMLException e){
            
        }
    }
}