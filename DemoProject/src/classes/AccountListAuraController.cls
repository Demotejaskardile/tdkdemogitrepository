public with sharing class AccountListAuraController {

    @AuraEnabled
    public static List<Account> getAccounts()
    {
        return [Select id, Name, Description, Website from Account ORDER by Name];
    }
}