public class ObjectListAuraController {
    
    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
    public class Pair
    {
        @AuraEnabled public String key {get; set;}
        @AuraEnabled public String val {get; set;}
    }
    
    public static String blobToString(Blob input, String inCharset){
        String hex = EncodingUtil.convertToHex(input);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    }   
    @AuraEnabled
    public static String saveTheFile(String fileName, String base64Data, String contentType) { 
       
        //List<sObject> deserializedInvoices = (List<sObject>)JSON.deserialize(base64Data, List<sObject>.class);
         
        
        //List<sObject> lstobj =(List<sObject>)System.JSON.deserialize(base64Data,List<sObject>.class);
        //System.debug('===base64Data===='+lstobj);
        //base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
       // String nameFile =blobToString( base64Data,'ISO-8859-1');
        List<Sobject> recordstoupload= new List<Sobject>();
        try{
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
           //string str = EncodingUtil.base64Decode(base64Data);
            String[] filelines = base64Data.split('\n');
            System.debug('===fileName===='+fileName);
            System.debug('=====filelines=='+filelines);
            Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
            Schema.SObjectType objecttypeinfo=gd.get(fileName);
           
            List<String> headerValues =new List<String>();
            headerValues = filelines[0].trim().split(',');
            System.debug('=====headerValues=='+headerValues);
            for (Integer i=1;i<filelines.size();i++)
            {
                String[] inputvalues = new String[]{};
                    inputvalues = filelines[i].split(',');
                sObject sObj = Schema.getGlobalDescribe().get(fileName).newSObject() ;  
                for(Integer j=0;j<headerValues.size();j++){
                    sObj.put(headerValues[j],inputvalues[j]) ;  
                }
                recordstoupload.add(sObj);
            }
        }
        catch(Exception e){
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured reading the CSV file'+e.getMessage());
            ApexPages.addMessage(errormsg);
            return 'An error has occured reading the CSV file';
        } 
        //Finally, insert the collected records
        try{
            if(recordstoupload.size()>0)
                insert recordstoupload;
            return 'Success';
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured inserting the records'+e.getMessage());
            ApexPages.addMessage(errormsg);
            return 'An error has occured inserting the records';
        }  
        
    }
    
    @AuraEnabled
    public static List<string>  getfieldlist(string selectedobj) {
        List<Pair> lstfieldname;
        List<String> objfieldLabels = new List<String>();
        Map<String, String> objLabelNameMap = new Map<String, String>();
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(selectedobj).getDescribe().fields.getMap();
        for(Schema.SObjectField sfield : fieldMap.Values())
        {
            schema.describefieldresult dfield = sfield.getDescribe();
            /*Pair field = new Pair();
            field.key = dfield.getname();
            field.val = dfield.getType () + ' : ' + dfield.getLabel ();
            lstfieldname.add(field);*/
            objfieldLabels.add(dfield.getLabel());
        }
        system.debug('------['+objfieldLabels+']');
        return objfieldLabels;
    }
    
    
    @AuraEnabled
    public static List < String > getObjectNames() {
        Map<String, String> objLabelNameMap = new Map<String, String>();
        List<String> objLabels = new List<String>();
        for (Schema.SObjectType item1: Schema.getGlobalDescribe().values()) {
            String name = item1.getDescribe().getName();
            // Exclude all the unwanted Sobjects e.g. CustomSettings, History, Share, Feed, ApexClass, Pages etc..
            if (!item1.getDescribe().isCustomSetting() && item1.getDescribe().getRecordTypeInfos().size() > 0 && item1.getDescribe().isCreateable() &&
                !name.containsignorecase('history') && !name.containsignorecase('tag') && !name.containsignorecase('share') && !name.containsignorecase('feed')) {
                    //options.add(new SelectOption(item1.getDescribe().getName(), item1.getDescribe().getLabel()));
                    objLabels.add(item1.getDescribe().getLabel());
                    objLabelNameMap.put(item1.getDescribe().getLabel(),
                                item1.getDescribe().getName());
                }
        }
        
       objLabels.sort();
        return objLabels;
    }
    
}